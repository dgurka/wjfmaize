<?PHP

$pgid = 1;
$contact = "no";
include("includes/header.inc.php");

?>
<table class="schedule" cellspacing="1" callpadding="1" border="1">
  <tr>
    <th>Date</th>
    <th>Home</th>
    <th>Away</th>
    <th>Location</th>
    <th>Time</th>
    <th>Order</th>
  </tr>
  <tr>
    <td>Sat, Sept. 7</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image8.png" />
      Dearborn Heights</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image4.png" />
      Washtenaw Maize</td>
    <td>Away</td>
    <td>11:00a.m.</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sat, Sept. 14</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image4.png" />
      Washtenaw Maize</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image10.jpeg" />
      Allen Park*</td>
    <td>Home</td>
    <td>3:00p.m.</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sat. Sept 21</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image7.png" />
      Wyandotte</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image4.png" />
      Washtenaw Maize</td>
    <td>Away</td>
    <td>11:00a.m.</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sat., Sept. 28</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image6.png" />
      Grosse Ile</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image4.png" />
      Washtenaw Maize</td>
    <td>Away</td>
    <td>11:00a.m.</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sat., Oct 5</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image4.png" />
      Washtenaw Maize</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image5.png" />
      Taylor-Cobras**</td>
    <td>Home</td>
    <td>11:00a.m.</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sat., Oct. 12</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image4.png" />
      Washtenaw Maize</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image2.png" />
      River Rouge</td>
    <td>Home</td>
    <td>11:00a.m.</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sat., Oct. 19</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image3.png" />
      Riverview</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image4.png" />
      Washtenaw Maize</td>
    <td>Away</td>
    <td>11:00a.m.</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sat., Oct. 26</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image4.png" />
      Washtenaw Maize</td>
    <td><img style="width: auto; height: 39px;" src="images/helmets/image4.png" />
      Washtenaw Blue</td>
    <td>Home</td>
    <td>11:00a.m.</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sat., Nov. 2</td>
    <td colspan="2"><img style="width: auto; height: 39px;" src="images/helmets/image9.gif" />
      AFC Playoff</td>
    <td>TBD</td>
    <td>TBD</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sun., Nov. 3</td>
    <td colspan="2"><img style="width: auto; height: 39px;" src="images/helmets/image11.png" />
      DJFL Cheer Comp.</td>
    <td>Lincoln</td>
    <td>10:00a.m.</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sat., Nov 9</td>
    <td colspan="2"><img style="width: auto; height: 39px;" src="images/helmets/image9.gif" />
      AFC Championship</td>
    <td>TBD</td>
    <td>TBD</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
  <tr>
    <td>Sat., Nov 16</td>
    <td colspan="2"><img style="width: auto; height: 39px;" src="images/helmets/image9.gif" />
      DJFL Super Bowl</td>
    <td>TBD</td>
    <td>TBD</td>
    <td>F-JV-Var</td>
  </tr>
  <tr>
    <td><b>Results:</b></td>
    <td colspan="5"></td>
  </tr>
</table>

<p>* Parent Appreciation<br/>** Homecoming Game</p>

<?PHP include("includes/footer.inc.php"); ?>