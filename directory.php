<?PHP
/****************************************
#	directory.php						#
#	Date Updated: 2/24/2012				#
****************************************/

$contact = "no";

$pagetitle = "Business Directory";

include("includes/header.inc.php");

if($config['bizdir'] == "off"){
	die(); 
}

echo "<h1 style='margin-bottom:0px;'>Business Directory</h1>";
echo "<div style='width:100%; margin-bottom:20px; font-style:italic; font-size:12px;'>(Businesses are listed alphabetically)</div>";
			
			$SQL = "SELECT * FROM directory ORDER BY title";
			$result = mysql_query($SQL);
			
			echo '<table style="width:100%;" cellpadding="5px">';
			
			$counter = 0;
			$cells_per_row = 2; 
			
			while ($db_field = mysql_fetch_assoc($result)) {
			     $counter++;
				if(($counter % $cells_per_row) == 1)  { echo '<tr>'; }
				echo '<td style="width:50%;" align="left" valign="top"><img src="'.$db_field["content"].'" style="width:125px; margin-right:5px; margin-bottom:20px;" align="left"/><strong>' . $db_field['title'] . '</strong><br />
				'.$db_field['address'];

				echo '<div style="text-align:right; margin-right:5px;">';
				if($db_field['url'] !==""){  echo '<br /><a href="'.$db_field['url'].'" target="_blank">Website</a>'; }
				
				if($db_field['url'] && $db_field['facebook'] !==""){ 
					echo ' | <a href="'.$db_field['facebook'].'" target="_blank">Facebook URL</a>'; 
				} else if($db_field['facebook'] !==""){ 
					echo '<a href="'.$db_field['facebook'].'" target="_blank">Facebook URL</a>'; 
				}
				
				echo '</div></td>';

				//echo '</div>';
				//echo '</td>';

				//echo '<div style="width:100%; height:20px;">&nbsp;</div></td>';
				if(($counter %  $cells_per_row) == 0) { echo '</tr>'; } 
				
				
				/*
				echo("<div class=\"glidecontent\">");
				echo("<div><a href='".$db_field['url']."' target='_blank'><img src='".$db_field['content']."'/></a></div>");
				echo("<div style=\"font-weight:bold;\">".$db_field['title']."</div>");
				echo("</div>");
				*/
			}			

	// just in case we haven't closed the last row
	// this would happen if our result set isn't divisible by $cells_per_row
	if(($counter % $cells_per_row) != 0) { echo '</tr>'; }
	
	echo '</table>'; 

	/*   <table style="width:725px;" border="0" align="center" cellspacing="5">

  <tr>

    <td align="left" valign="top"><?PHP getbox(1,2); ?></td>

    <td align="left" valign="top"><?PHP getbox(2,2); ?></td>

  </tr>

  <tr>

    <td align="left" valign="top"><?PHP getbox(3,2); ?></td>

    <td align="left" valign="top"><?PHP getbox(4,2); ?></td>

  </tr>

  <tr>

    <td align="left" valign="top"><?PHP getbox(5,2); ?></td>

    <td align="left" valign="top"><?PHP getbox(6,2); ?></td>

  </tr>

</table>
        */

include("includes/footer.inc.php");



?>
