<?PHP
/****************************************
#	msgboard.php						#
#	Date Updated: 8/26/2011				#
****************************************/


$pgid = "1";

include("includes/header.inc.php");

$privatekey = "6Lf9f8cSAAAAANkE5QI6nrR53Wli1Uw3KjchCMxm";
$publickey = "6Lf9f8cSAAAAAGRK4LJhODupf2q_RK1FCTvpflvW";

if($config['msgboard'] == "off"){
	die(); 
}
?>
<h1>Message Board</h1>

<?php 

if($_POST['Submit']){

	require_once('includes/recaptchalib.php');
	
	$resp = recaptcha_check_answer($privatekey,$_SERVER["REMOTE_ADDR"],$_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);



	if (!$resp->is_valid) {
		// What happens when the CAPTCHA was entered incorrectly
		echo("<h2 style='color:red'>The reCAPTCHA wasn't entered correctly. Go back and try it again.</h2>");

	} else {
		
		$datetime = date("F j, Y, g:i a");
		$IP = @$_SERVER['REMOTE_ADDR'];  
		
		$deny = array("24.185.119.190","24.247.106.78","24.18.81.13","67.10.227.24","75.70.154.10");
		if (in_array ($IP, $deny)) {
			die("<h2 style='color:red'>Your IP address, ".$IP." has been banned for abuse of this system.</h2>");
		}
		
		$name =  sterilize($_POST['name']);
		$email =  sterilize($_POST['email']);
		$comment =  sterilize($_POST['comment']);
		
		define('REGEX_URL','/http(s)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/'); // 1st check of http:// 

		
		if(preg_match(REGEX_URL, $comment, $url)) {
			die("<h2 style='color:red'>Sorry, due to abuse of this system, posting links is not allowed.</h2>");
		}

		define('REGEX_URL2','@((www|http://)[^ ]+)@');

		if(preg_match(REGEX_URL2, $comment, $url)) {
			die("<h2 style='color:red'>Sorry, due to abuse of this system, posting links is not allowed.</h2>");
		}
		
		$sql="INSERT INTO msgboard(name, email, comment, datetime,IP,approved)VALUES('$name', '$email', '$comment', '$datetime','$IP','true')";
		
		$result = mysql_query($sql);
		
		$to_email = $config['adminemail'];
		$subject = $config['sitename']." Message Board Post";
		
		$body = "Name: ".$name."\n E-Mail: ".$email."\n Message:\n ".$comment."\n  IP Address: ".$IP."\n\n If inappropriate, this message can be deleted using the message board control panel: ".$config['url']."admin/msgboard.php";			
		
		mail($to_email, $subject, $body, "From: noreply@".$config['domain']);
		mail("frank@enablepoint.com,don@enablepoint.com", $subject, $body, "From: noreply@".$config['domain']);
		
		
		
		//check if query successful
		if($result){
			echo "<h2 style='color:green'>Your post was successful.</h2>";
		} else {
			echo "<h2 style='color:red'>ERROR</h2>";
		}
	}
}



?>
  
<form method="post" action="msgboard.php">

<table width="416" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
<tr>
<td width="89">Name</td>
<td width="5">:</td>
<td width="300"><input name="name" type="text" id="name" size="40" /></td>
</tr>
<tr>
<td>Email (will not be displayed)</td>
<td>:</td>
<td><input name="email" type="text" id="email" size="40" /></td>
</tr>
<tr>
<td valign="top">Message</td>
<td valign="top">:</td>
<td><textarea name="comment" cols="40" rows="3" id="comment"></textarea></td>
</tr>
<tr>
  <td colspan="3" align="center" valign="top">Please enter the text you see in the box below<br />
    <br />    <?php
          require_once('includes/recaptchalib.php');
          echo recaptcha_get_html($publickey);
        ?>
</td>
  </tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><input type="submit" name="Submit" value="Submit" /> <input type="reset" name="Reset" value="Reset" /></td>
</tr>
</table>
</form>
<div style="width:100%; height:15px;"></div>
<?php
// find out how many rows are in the table 
$sql3 = "SELECT COUNT(*) FROM msgboard WHERE approved = 'true'";
$result3 = mysql_query($sql3);
$r = mysql_fetch_row($result3);
$numrows = $r[0];

// number of rows to show per page
$rowsperpage = 10;
// find out total pages
$totalpages = ceil($numrows / $rowsperpage);

// get the current page or set a default
if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
   // cast var as int
   $currentpage = (int) $_GET['currentpage'];
} else {
   // default page num
   $currentpage = 1;
} // end if

// if current page is greater than total pages...
if ($currentpage > $totalpages) {
   // set current page to last page
   $currentpage = $totalpages;
} // end if
// if current page is less than first page...
if ($currentpage < 1) {
   // set current page to first page
   $currentpage = 1;
} // end if

// the offset of the list, based on current page 
$offset = ($currentpage - 1) * $rowsperpage;



$sql2 = "SELECT * FROM msgboard WHERE approved = 'true' ORDER BY id DESC LIMIT $offset, $rowsperpage;";
$result2 = mysql_query($sql2);

/******  build the pagination links ******/
// range of num links to show
$range = 3;

// if not on page 1, don't show back links
if ($currentpage > 1) {
   // show << link to go back to page 1
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=1'>&laquo; Beginning</a> ";
   // get previous page num
   $prevpage = $currentpage - 1;
   // show < link to go back to 1 page
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage'>&#8249; Back</a> ";
} // end if 

// loop to show links to range of pages around current page
for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
   // if it's a valid page number...
   if (($x > 0) && ($x <= $totalpages)) {
      // if we're on current page...
      if ($x == $currentpage) {
         // 'highlight' it but don't make a link
         echo " [<b>$x</b>] ";
      // if not current page...
      } else {
         // make it a link
         echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$x'>$x</a> ";
      } // end else
   } // end if 
} // end for
                 
// if not on last page, show forward and last page links        
if ($currentpage != $totalpages) {
   // get next page
   $nextpage = $currentpage + 1;
    // echo forward link for next page 
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage'>Next &#8250;</a> ";
   // echo forward link for lastpage
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages'>End &raquo;</a> ";
} // end if
/****** end build pagination links ******/


while($rows=mysql_fetch_array($result2)){
?>
<table width="400" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
<tr>
<td><table width="400" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
<tr>
<td width="117">Name</td>
<td width="14">:</td>
<td width="357"><?PHP echo $rows['name']; ?></td>
</tr>
<tr>
<td valign="top">Message</td>
<td valign="top">:</td>
<td><?PHP echo $rows['comment']; ?></td>
</tr>
<tr>
<td valign="top">Date/Time </td>
<td valign="top">:</td>
<td><?PHP echo $rows['datetime']; ?></td>
</tr>
</table></td>
</tr>
</table>
<BR>
<?
}

/******  build the pagination links ******/
// range of num links to show
$range = 3;

// if not on page 1, don't show back links
if ($currentpage > 1) {
   // show << link to go back to page 1
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=1'><<</a> ";
   // get previous page num
   $prevpage = $currentpage - 1;
   // show < link to go back to 1 page
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage'><</a> ";
} // end if 

// loop to show links to range of pages around current page
for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
   // if it's a valid page number...
   if (($x > 0) && ($x <= $totalpages)) {
      // if we're on current page...
      if ($x == $currentpage) {
         // 'highlight' it but don't make a link
         echo " [<b>$x</b>] ";
      // if not current page...
      } else {
         // make it a link
         echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$x'>$x</a> ";
      } // end else
   } // end if 
} // end for
                 
// if not on last page, show forward and last page links        
if ($currentpage != $totalpages) {
   // get next page
   $nextpage = $currentpage + 1;
    // echo forward link for next page 
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage'>></a> ";
   // echo forward link for lastpage
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages'>>></a> ";
} // end if
/****** end build pagination links ******/


include("includes/footer.inc.php"); ?>