<?php

include("includes/header.inc.php");

function build_calendar($month,$year,$events) {

     // Create array containing abbreviations of days of week.
     $daysOfWeek = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

     // What is the first day of the month in question?
     $firstDayOfMonth = mktime(0,0,0,$month,1,$year);

     // How many days does this month contain?
     $numberDays = date('t',$firstDayOfMonth);

     $_month = (strlen($month) > 1) ? $month : "0" . $month;

     $_bdate = "{$year}-{$_month}-";
     $_sdate = "{$year}-{$_month}-1";
     $_edate = "{$year}-{$_month}-{$numberDays}";
     
     $sql = <<<EOT
SELECT ID, title, start_date FROM event 
WHERE start_date >= '{$_sdate}' AND start_date <= '{$_edate}'
ORDER BY start_date, start_time
EOT;

     
     $result = mysql_query($sql) or die(mysql_error());

     $events = array();
     while($row = mysql_fetch_assoc($result))
     {
          $key =  ltrim(str_replace($_bdate, "", $row["start_date"]), "0");
          if(isset($events[$key]))
          {
               $events[$key] .= "<br/>";
          }
          else
          {
               $events[$key] = "";
          }
          $events[$key] .= "<a href='event.php?evid={$row["ID"]}'>" . $row["title"] . "</a>";
     }
     /*var_dump($sql);
     exit();*/
     // Retrieve some information about the first day of the
     // month in question.
     $dateComponents = getdate($firstDayOfMonth);

     // What is the name of the month in question?
     $monthName = $dateComponents['month'];

     // What is the index value (0-6) of the first day of the
     // month in question.
     $dayOfWeek = $dateComponents['wday'];

     // Create the table tag opener and day headers

     $calendar = "<table border=\"1\" bordercolordark=\"#000000\" bordercolorlight=\"#FFFFFF\" cellpadding=\"5\" class='calendar'>";
     $calendar .= "<caption><b>$monthName $year</b></caption>";
     $calendar .= "<tr>";

     // Create the calendar headers

     foreach($daysOfWeek as $day) {
          $calendar .= "<th class='header'>$day</th>";
     } 

     // Create the rest of the calendar

     // Initiate the day counter, starting with the 1st.

     $currentDay = 1;

     $calendar .= "</tr><tr>";

     // The variable $dayOfWeek is used to
     // ensure that the calendar
     // display consists of exactly 7 columns.

     if ($dayOfWeek > 0) { 
          $calendar .= "<td valign='top' colspan='$dayOfWeek'>&nbsp;</td>"; 
     }
     
     $month = str_pad($month, 2, "0", STR_PAD_LEFT);
  
     while ($currentDay <= $numberDays) {

          // Seventh column (Saturday) reached. Start a new row.

          if ($dayOfWeek == 7) {

               $dayOfWeek = 0;
               $calendar .= "</tr><tr>";

          }
          
          $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);
          
          $date = "$year-$month-$currentDayRel";
          if(isset($events[(string)$currentDay])) {
               $_ev = $events[(string)$currentDay];
               $calendar .= "<td valign='top' class='day' rel='$date'>$currentDay<br/>$_ev</td>";
          }
          else {
               $calendar .= "<td valign='top' class='day' rel='$date'>$currentDay<br/><br/><br/></td>";
          }

          // Increment counters
 
          $currentDay++;
          $dayOfWeek++;

     }
     
     

     // Complete the row of the last week in month, if necessary

     if ($dayOfWeek != 7) { 
     
          $remainingDays = 7 - $dayOfWeek;
          $calendar .= "<td colspan='$remainingDays'>&nbsp;</td>"; 

     }
     
     $calendar .= "</tr>";

     $calendar .= "</table>";

     return $calendar;

}?>

<?php
if(isset($_GET["month"]) && isset($_GET["year"]))
{
     $month = $_GET['month'];                  
     $year = $_GET['year'];
}
else
{
     $dateComponents = getdate();

     $month = $dateComponents['mon'];                  
     $year = $dateComponents['year'];
}


$events = array();
$events["1"] = "An Event";

$prevlink = "";
$nextlink = "";

$_m = (int)$month;
$_y = (int)$year;

$prev_month = $_m - 1;
$prev_year = $_y;
if($prev_month == 0)
{
     $prev_month = 12;
     $prev_year -= 1;
}

$next_month = $_m + 1;
$next_year = $_y;
if($next_month == 13)
{
     $next_month = 1;
     $next_year += 1;
}

$prevlink = "?month={$prev_month}&year={$prev_year}";
$nextlink = "?month={$next_month}&year={$next_year}";
?>
<center><a href="<?php echo $prevlink; ?>">&lt;-- Previous</a> &nbsp; &nbsp; &nbsp; &nbsp; <a href="<?php echo $nextlink; ?>">Next --&gt;</a></center>
<?php echo build_calendar($month,$year,$events); ?>
<center><a href="<?php echo $prevlink; ?>">&lt;-- Previous</a> &nbsp; &nbsp; &nbsp; &nbsp; <a href="<?php echo $nextlink; ?>">Next --&gt;</a></center>


<?PHP include("includes/footer.inc.php"); ?>