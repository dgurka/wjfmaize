<?PHP
/****************************************
#	global_functs.php					#
#	Date Updated: 7/22/2011				#
****************************************/

// Database function: 
// Connects to the database
// EXAMPLE USAGE: 
// dbconn("localhost", "username", "password", "databasename");
function dbconn($dbhost, $dbuser, $dbpass, $dbname){
	$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die ('Error connecting to mysql');
	mysql_select_db($dbname);
}

function sterilize ($input, $is_sql = false)
{
    $input = htmlentities($input, ENT_QUOTES);

    if(get_magic_quotes_gpc ())
    {
        $input = stripslashes ($input);
    }

    if ($is_sql)
    {
        $input = mysql_real_escape_string ($input);
    }

    $input = strip_tags($input);
    $input = str_replace("
", "\n", $input);

    return $input;
}


function leftnav(){	
	$query  = "SELECT * FROM epitest_pages";
	$result = mysql_query($query);
	echo("<ul>");
	while($row = mysql_fetch_array($result))
	{
		echo("<li><a href=\"page.php?id=".$row['id']."\">".$row['title']."</a></li>");
	} 
	echo("</ul>");
}


function getPageTitle($pagenum){
// Make a MySQL Connection
	$query = "SELECT * FROM epitest_pages WHERE id = '$pagenum'"; 
	$result = mysql_query($query) or die(mysql_error());
		
	$row = mysql_fetch_array($result) or die(mysql_error());

		echo " - " .$row['title'];
}

function getPageTitle2($pagenum){
// Make a MySQL Connection
	$query = "SELECT * FROM epitest_pages WHERE id = '$pagenum'"; 
	$result = mysql_query($query) or die(mysql_error());
		
	$row = mysql_fetch_array($result) or die(mysql_error());

		echo $row['title'];
}


function getbox($boxnum, $pagenum){
// Make a MySQL Connection
	$query = "SELECT * FROM epitest_boxes WHERE boxnum = '$boxnum' AND pagenum = '$pagenum'"; 
	$result = mysql_query($query) or die(mysql_error());
		
	$row = mysql_fetch_array($result) or die(mysql_error());
	if($row['imgtxt'] == "txt" || $row['imgtxt'] == "fb"){
		echo $row['content'];
	} else if($row['imgtxt'] == "img"){
		echo("<img src='".$row['imgpath']."' />"); 
	} else if($row['imgtxt'] == "you"){
		embed($row['yturl']); 
	}
}

function getkeywords($pagenum){
// Make a MySQL Connection
	$query = "SELECT keywords FROM epitest_pages WHERE id = '$pagenum'"; 
	$result = mysql_query($query) or die(mysql_error());
		
	$row = mysql_fetch_array($result) or die(mysql_error());
	
	echo("<meta name='keywords' content='".$row['keywords']."'>");
	
}

   
function embed($raw)
{      
  //clean the rawdata
  $raw = trim($raw);
  
  //get the code
  $hosts = array("http://youtube.com/watch?v=", "http://www.youtube.com/watch?v=", "http://youtu.be/", "http://video.google.com/videoplay?docid=-");
  $code = str_replace($hosts, "", $raw);
  

  //find host
  $host1 = strpos($raw, "youtube.com", 1);
  $host2 = strpos($raw, "video.google.com", 1);
  $host3 = strpos($raw, "youtu.be", 1);
  
  if($host1 != null) {$host .= "youtube";}
  if($host2 != null) {$host .= "google";}
  if($host3 != null) {$host .= "youtube";}
  
  //error
  if($host != "youtube" && $host != "google")
  {
	 $error = true;
	 echo "Error Embedding video";
  }
  
  if($host == "youtube")
  {
	 echo "<object width='425' height='353'><param name='movie' value='http://www.youtube.com/v/".$code."&rel=0'></param>
	 <param name='wmode' value='transparent'></param>
	 <embed src='http://www.youtube.com/v/".$code."&rel=0' type='application/x-shockwave-flash' wmode='transparent' width='425' height='353'>
	 </embed></object><br/>";
  }
  
  if($host == "google")
  {
	 echo "<embed style='width:425px; height:353px;' id='VideoPlayback' type='application/x-shockwave-flash'
	 src='http://video.google.com/googleplayer.swf?docId=-$code&hl=en' flashvars=''></embed><br/><br/>";
  }
  
}

?>