<?PHP
session_start();
include("includes/functions.php");
include("../includes/config.php");

if(isset($_POST['login']))
{
	$pswd = clean($_POST['pswd']);
	$usrnm = clean($_POST['usrnm']);
	$username = $config['username'];
	$password = $config['password'];
	login($usrnm, $pswd, $username, $password);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Login to <?PHP echo $config['sitename']; ?></title>
</head>
<body>
    <div align="center">
        <h2>Login to <?PHP echo $config['sitename']; ?>:</h2>
        <form method="post" action="<?PHP $PHP_SELF; ?>">
        Username: <input type="text" name="usrnm"><br />
        Password: <input type="password" name="pswd"><br />
        <input type="submit" name="login" value="Login">
        </form>
    </div>
</body>
</html>