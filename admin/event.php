<?PHP
/****************************************
#	Inventory.php						#
#	Date Updated: 2/7/2012				#
****************************************/

session_start();

include("includes/functions.php");

include("../includes/global_functs.php");

include("../includes/config.php");

dbconn($db['host'], $db['username'], $db['password'], $db['database']);

check();

$title = "";
$imageloc = "";
$start_date = "";
$start_time = "";
$end_date = "";
$end_time = "";
$location = "";
$location_address = "";
$description = "";

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	foreach ($_POST as $key => $value) {
		if(isset($$key)){
			$$key = $value;
			$_t = "_" . $key;
			$$_t = $value;
		}
	}

	if($_start_date != "")
	{
		$_t = strtotime($_start_date);
		$_start_date = "'" . date("Y-m-d", $_t) . "'";
	}
	else
	{
		$_start_date = "NULL";
	}

	if($_start_time != "")
	{
		$_t = strtotime($_start_time);
		$_start_time = "'" . date("H:i:s", $_t) . "'";
	}
	else
	{
		$_start_time = "NULL";
	}

	if($_end_date != "")
	{
		$_t = strtotime($_end_date);
		$_end_date = "'" . date("Y-m-d", $_t) . "'";
	}
	else
	{
		$_end_date = "NULL";
	}

	if($_end_time != "")
	{
		$_t = strtotime($_end_time);
		$_end_time = "'" . date("H:i:s", $_t) . "'";
	}
	else
	{
		$_end_time = "NULL";
	}

	$file = $_FILES["image_up"];
	$has_image = false;
	if($file["error"] > 0)
	{

	}
	else
	{
		$has_image = true;
		$fname = uniqid() . "." . $file["name"];
		$base = dirname(__DIR__) . "/";
		$loc = "inventory/events/";
		move_uploaded_file($file["tmp_name"],
	      $base . $loc . $fname);

		$imageloc = $loc . $fname;
	}

	if(isset($_GET["id"]))
	{
		// update
		$id = (int)$_GET["id"];
		if($has_image)
			$sql = "UPDATE event SET title = '$_title', imageloc = '$imageloc', start_date = $_start_date, start_time = $_start_time, end_date = $_end_date, end_time = $_end_time, location = '$_location', location_address = '$_location_address', description = '$_description' WHERE ID = $id";
		else
			$sql = "UPDATE event SET title = '$_title', start_date = $_start_date, start_time = $_start_time, end_date = $_end_date, end_time = $_end_time, location = '$_location', location_address = '$_location_address', description = '$_description' WHERE ID = $id";

		mysql_query($sql);
		if(mysql_errno()) die(mysql_error());
		header("Location: event.php?id=$id&success=1");
	}
	else
	{
		// insert
		if($has_image)
			$sql = "INSERT INTO event (title, imageloc, start_date, start_time, end_date, end_time, location, location_address, description) VALUES ('$_title', '$imageloc', $_start_date, $_start_time, $_end_date, $_end_time, '$_location', '$_location_address', '$_description')";
		else
			$sql = "INSERT INTO event (title, start_date, start_time, end_date, end_time, location, location_address, description) VALUES ('$_title', $_start_date, $_start_time, $_end_date, $_end_time, '$_location', '$_location_address', '$_description')";

		mysql_query($sql);
		$id = mysql_insert_id();
		header("Location: event.php?id=$id&success=1");
	}
}

else if(isset($_GET["id"]))
{
	$id = (int)$_GET["id"];
	$sql = <<<EOT
	SELECT * FROM event
	WHERE ID = $id
EOT;

	$result = mysql_query($sql) or die(mysql_error());

	if($row = mysql_fetch_assoc($result))
	{
		foreach ($row as $key => $value) {
			if(isset($$key))
				$$key = $value;
		}
	}

	if($start_date != null)
	{
		$_t = strtotime($start_date);
		$start_date = date("m/d/Y", $_t);
	}
	if($start_time != null)
	{
		$_t = strtotime($start_time);
		$start_time = date("g:i A", $_t);
	}	

	if($end_date != null)
	{
		$_t = strtotime($end_date);
		$end_date = date("m/d/Y", $_t);
	}

	if($end_time != null)
	{
		$_t = strtotime($end_time);
		$end_time = date("g:i A", $_t);
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?PHP echo $config['sitename']; ?> Administration Control Panel</title>

<?PHP	include("./includes/tinymce.php"); ?>


<style>

table td {
	vertical-align: top;
}
</style>
</head>

<?php if(isset($_GET["id"])): ?>
<script type="text/javascript">

function doDelete()
{
	if(confirm('Are you sure?'))
	{
		window.location="delete_event.php?id=<?php echo $_GET['id']; ?>";
	}
	else
	{
		return false;
	}
}

</script>
<?php endif; ?>
<body>

<div><center><h2><?PHP echo $config['sitename']; ?> Administration Control Panel - Manage Event</h2></center><br />
<?php if(isset($_GET["success"])) : ?>
<span style="color:green">Successfully Updated</span>
<?php endif; ?>
<form enctype="multipart/form-data" method="post">
<table>
	<tr>
		<td>Title:</td>
		<td><input name="title" type="text" value="<?php echo $title ?>" /></td>
	</tr>
	<tr>
		<td>Image:</td>
		<td><input name="image_up" type="file" /> <?php echo $imageloc ?></td>
	</tr>
	<tr>
		<td>Start Date:</td>
		<td><input name="start_date" type="text" value="<?php echo $start_date ?>" /></td>
	</tr>
	<tr>
		<td>Start Time:</td>
		<td><input name="start_time" type="text" value="<?php echo $start_time ?>" /></td>
	</tr>
	<tr>
		<td>End Date:</td>
		<td><input name="end_date" type="text" value="<?php echo $end_date ?>" /></td>
	</tr>
	<tr>
		<td>End Time:</td>
		<td><input name="end_time" type="text" value="<?php echo $end_time ?>" /></td>
	</tr>
	<tr>
		<td>Location:</td>
		<td><input name="location" type="text" value="<?php echo $location ?>" /></td>
	</tr>
	<tr>
		<td>Location Address:</td>
		<td><textarea name="location_address"><?php echo $location_address ?></textarea></td>
	</tr>
	<tr>
		<td>Description:</td>
		<td><textarea name="description"><?php echo $description ?></textarea></td>
	</tr>
</table>

<input type="submit" value="Save" /> 
</form>
<?php if(isset($_GET["id"])): ?>
<input type="button" onclick="doDelete();" value="Delete Event" />
<?php endif; ?>
  <hr>
	<a href="index.php">Return to Menu</a> | <a href="events.php">Return to Events</a> | <a href="logout.php">Logout</a>
</div>

</body>

</html>
