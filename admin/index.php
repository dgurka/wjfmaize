<?PHP

session_start();

include("includes/functions.php");

include("../includes/global_functs.php");

include("../includes/config.php");

dbconn($db['host'], $db['username'], $db['password'], $db['database']);



check();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?PHP echo $config['sitename']; ?> Administration Control Panel</title>

<?PHP include("./includes/tinymce.php"); ?>



</head>



<body>

<div align='center'><h2><?PHP echo $config['sitename']; ?> Administration Control Panel</h2><br />

(images are recommended to be less than 275x275 pixels in size)</div>

<?PHP 



$act = (isset($_GET['act'])) ? $_GET['act'] : null;

switch ($act)

{

	case 'editpage':

		$pagename = $_GET['pagename'];

		$pageid = $_GET['id'];

		echo("<h2>Editing Page: ".$_GET['pagename']."</h2>");

		admingetbox(1, $pageid);

		admingetbox(2, $pageid);

		admingetbox(3, $pageid);

		admingetbox(4, $pageid);

		admingetbox(5, $pageid);

		admingetbox(6, $pageid);

		break;

	case 'editbox':

		$boxnum = $_GET['boxnum'];

		$pagenum = $_GET['pagenum'];

		$pagename = $_GET['pagename'];

		echo("<h2>Editing Box #: ".$boxnum." on Page: ".$pagename." (# ".$pagenum.")</h2>");

		admineditbox($boxnum, $pagenum);

		break;

	case 'updatebox':

		$boxnum = mysql_real_escape_string($_GET['boxnum']);

		$pagenum = mysql_real_escape_string($_GET['pagenum']);

		$content = mysql_real_escape_string($_POST['textarea']);

		$imgtxt = mysql_real_escape_string($_POST['radio']);

		$yturl = mysql_real_escape_string($_POST['youtube']);

		updatebox($boxnum, $pagenum, $content, $imgtxt, $yturl);

		listpages();

		break;

	case 'keywords':

		$pagenum = $_GET['id'];

		$pagename = $_GET['pagename'];

		echo("<h2>Editing Keywords on Page: ".$pagename."</h2>");

		adminkeywords($pagenum);

		break;

	case 'updatekeywords';

		$pagenum = $_GET['pagenum'];

		$keywords = $_POST['keywords'];

		updatekeywords($pagenum, $keywords);

		listpages();

		break;

	default:

		listpages();

		break;

} 		

		echo("<hr>");

		echo("<a href=\"index.php\">Return to Menu</a> | <a href=\"logout.php\">Logout</a>");



?>

</body>

</html>