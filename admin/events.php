<?PHP
/****************************************
#	Inventory.php						#
#	Date Updated: 2/7/2012				#
****************************************/

session_start();

include("includes/functions.php");

include("../includes/global_functs.php");

include("../includes/config.php");

dbconn($db['host'], $db['username'], $db['password'], $db['database']);

check();

$sql = <<<EOT
SELECT ID, title, start_date FROM event
ORDER BY start_date DESC, start_time DESC
EOT;

$result = mysql_query($sql) or die(mysql_error());

$events = array();
while($row = mysql_fetch_assoc($result))
{
	$events[$row["ID"]] = $row["title"] . " - " . date("m/d/Y", strtotime($row["start_date"]));
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?PHP echo $config['sitename']; ?> Administration Control Panel</title>

<?PHP	include("./includes/tinymce.php"); ?>



</head>



<body>

<div><center><h2><?PHP echo $config['sitename']; ?> Administration Control Panel - Manage Events</h2></center><br />
<ul>
<?php 
foreach ($events as $key => $value) :
?>
	<li><a href="event.php?id=<?php echo $key; ?>"><?php echo $value; ?></a></li>
<?php endforeach; ?>
</ul>
<button onclick="window.location='event.php'">Add New Event</button>
  <hr>
	<a href="index.php">Return to Menu</a> | <a href="logout.php">Logout</a>
</div>

</body>

</html>
