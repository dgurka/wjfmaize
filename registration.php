<?PHP
$noimg = "true";
$contact = "yes";
include("includes/header.inc.php"); ?>
<h1>Registration</h1>
<p>Important: A participant's parent/guardian must complete the Registration forms. Both an original and photocopy of the participant's birth certificate are required for registration for all players. The registration packet must be notarized, along with a physical signed + dated by a physician after April 15, 2012.</p>
<p>&nbsp;</p>
<table width="279" border="0" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td><a href="#"><img src="images/WJFMaizeRegistration_03.jpg" width="133" height="36" /></a></td>
    <td><a href="#"><img src="images/WJFMaizeRegistration_05.jpg" width="132" height="36" /></a></td>
  </tr>
  <tr>
    <td>Forms Required:</td>
    <td>Forms Required:</td>
  </tr>
  <tr>
    <td>1)</td>
    <td>1)</td>
  </tr>
  <tr>
    <td>2)</td>
    <td>2)</td>
  </tr>
  <tr>
    <td>3) </td>
    <td>3) </td>
  </tr>
</table>
<p>If you have any questions, contact the Registrar Lisa Edmondson at 734-635-1286 or email at edmondso31 [at] gmail.com</p>
<?PHP include("includes/footer.inc.php"); ?>